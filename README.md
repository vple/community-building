# Community Building

A topic that I'm pretty interested in is how to build communities. I hope to capture my learnings on this topic in this repo.

Today, I'm trying to build a software engineering team. A few years from now, I hope to build a company. Both of these are communities, but really any group of people that works, eats, or plays together is a community.

I don't know if there will be universal ideas or values that can permeate all communities. I suspect so. My focus will be on the specific experiences I have. As I gain more perspective, I'll hopefully be able to refine which ideas are important to communities in general.